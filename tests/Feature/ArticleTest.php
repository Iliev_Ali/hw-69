<?php
namespace Tests\Feature;

use App\Models\Article;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function test_success_create(): void
    {
        $user = User::factory()->create();
        Sanctum::actingAs($user);

        $data = [
            'title' => $this->faker->sentence(),
            'content' => $this->faker->paragraph(),
            'user_id' => $user->id
        ];

        $this->assertDatabaseMissing('articles', $data);

        $response = $this->postJson(
            route('articles.store'),
            $data,
            ['Authorization' => 'Bearer ' . $user->createToken('test')->plainTextToken]
        );

        $response->assertCreated();
        $payload = $response->collect()->toArray();
        $this->assertArrayHasKey('data', $payload);
        $this->assertArrayHasKey('id', $payload['data']);
        $this->assertArrayHasKey('title', $payload['data']);
        $this->assertArrayHasKey('author', $payload['data']);
        $this->assertArrayHasKey('id', $payload['data']['author']);
        $this->assertArrayHasKey('email', $payload['data']['author']);
        $this->assertArrayHasKey('comments', $payload['data']);

        $this->assertDatabaseHas('articles', $data);
        $this->assertEquals($data['title'], $payload['data']['title']);
    }

    /**
     * @group articles
     * @return void
     */
    public function test_success_update(): void
    {
        $user = User::factory()->create();
        Sanctum::actingAs($user);

        $article = Article::factory()->create([
            'title' => $this->faker->sentence(1, rand(3, 9)),
            'content' => $this->faker->paragraph(),
            'user_id' => $user->id
        ]);

        $response = $this->putJson(
            route('articles.update', $article->id), $article->toArray(),
            ['Authorization' => 'Bearer ' . $user->createToken('test')->plainTextToken]
        );

        $response->assertStatus(201);
        $payload = $response->collect()->toArray();
        $this->assertArrayHasKey('id', $payload);
        $this->assertArrayHasKey('title', $payload);
        $this->assertArrayHasKey('author', $payload);
        $this->assertArrayHasKey('id', $payload['author']);
        $this->assertArrayHasKey('email', $payload['author']);
        $this->assertArrayHasKey('comments', $payload);

    }

    /**
     * @group articlesDelete
     */
    public function test_success_destroy()
    {
        $user = User::factory()->create();
        Sanctum::actingAs($user);

        $article = Article::factory()->create([
            'title' => $this->faker->sentence(1, rand(3, 9)),
            'content' => $this->faker->paragraph(),
            'user_id' => $user->id
        ]);

        $response = $this->putJson(
            route('articles.update', $article->id), $article->toArray(),
            ['Authorization' => 'Bearer ' . $user->createToken('test')->plainTextToken]
        );
        $response->assertStatus(204);
        $payload = $response->collect()->toArray();
        $this->assertArrayHasKey('id', $payload);
        $this->assertArrayHasKey('title', $payload);
        $this->assertArrayHasKey('author', $payload);
        $this->assertArrayHasKey('id', $payload['author']);
        $this->assertArrayHasKey('email', $payload['author']);
        $this->assertArrayHasKey('comments', $payload);
    }


    /**
     * @group articleAuth
     * @return void
     */
    public function test_not_auth_user_trying_create_article(): void
    {
        $user = User::factory()->create();
        $article = Article::factory()->create([
            'title' => $this->faker->sentence(1, rand(3, 9)),
            'content' => $this->faker->paragraph(),
            'user_id' => $user->id
        ]);

        $response = $this->postJson(route('articles.store'), $article->toArray());
        $response->assertStatus(401);
        $response->assertUnauthorized();
    }

    /**
     * @group articleAuth
     * @return void
     */
    public function test_not_auth_user_trying_update_article(): void
    {
        $user = User::factory()->create();
        $article = Article::factory()->create([
            'title' => $this->faker->sentence(1, rand(3, 9)),
            'content' => $this->faker->paragraph(),
            'user_id' => $user->id
        ]);

        $response = $this->putJson(route('articles.update', $article->id), $article->toArray());
        $response->assertStatus(401);
        $response->assertUnauthorized();
    }

    /**
     * @group articleAuth
     * @return void
     */
    public function test_not_auth_user_trying_view_article(): void
    {
        $user = User::factory()->create();
        $article = Article::factory()->create([
            'title' => $this->faker->sentence(1, rand(3, 9)),
            'content' => $this->faker->paragraph(),
            'user_id' => $user->id
        ]);

        $response = $this->getJson(route('articles.show', $article->id), $article->toArray());
        $response->assertStatus(401);
        $response->assertUnauthorized();
    }

    /**
     * @group articleAuth
     * @return void
     */
    public function test_not_auth_user_trying_delete_article(): void
    {
        $user = User::factory()->create();
        $article = Article::factory()->create([
            'title' => $this->faker->sentence(1, rand(3, 9)),
            'content' => $this->faker->paragraph(),
            'user_id' => $user->id
        ]);

        $response = $this->deleteJson(route('articles.destroy', $article->id), $article->toArray());
        $response->assertStatus(401);
        $response->assertUnauthorized();
    }


}
