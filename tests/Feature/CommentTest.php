<?php

namespace Tests\Feature;

use App\Models\Comment;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class CommentTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @group comment
     * @return void
     */
    public function test_success_create(): void
    {
        $user = User::factory()->create();
        Sanctum::actingAs($user);

        $data = [
            'body' => $this->faker->paragraph(),
            'rating' => $this->faker->randomFloat(),
            'user_id' => $user->id
        ];

        $response = $this->postJson(
            route('comments.store'),
            $data,
            ['Authorization' => 'Bearer ' . $user->createToken('test')->plainTextToken]
        );

        $response->assertCreated();
        $response->assertStatus(201);
        $payload = $response->collect()->toArray();
        $this->assertArrayHasKey('data', $payload);
        $this->assertArrayHasKey('id', $payload['data']);
        $this->assertArrayHasKey('body', $payload['data']);
        $this->assertArrayHasKey('rating', $payload['data']);
    }

    /**
     * @group comment
     * @return void
     */
    public function test_success_update(): void
    {
        $user = User::factory()->create();
        Sanctum::actingAs($user);

        $comment = Comment::factory()->create([
            'body' => $this->faker->paragraph(),
            'rating' => $this->faker->randomFloat(),
            'user_id' => $user->id
        ]);

        $response = $this->putJson(
            route('comments.update', $comment->id),
            $comment->toArray(),
            ['Authorization' => 'Bearer ' . $user->createToken('test')->plainTextToken]
        );

        $response->assertStatus(201);
        $payload = $response->collect()->toArray();
        $this->assertArrayHasKey('id', $payload);
        $this->assertArrayHasKey('body', $payload);
        $this->assertArrayHasKey('rating', $payload);
    }

    /**
     * @group commentAuth
     * @return void
     */
    public function test_not_auth_user_trying_create_comment(): void
    {
        $user = User::factory()->create();
        $article = Comment::factory()->create([
            'body' => $this->faker->sentence(1, rand(3, 9)),
            'rating' => $this->faker->paragraph(),
            'user_id' => $user->id
        ]);

        $response = $this->postJson(route('comments.store'), $article->toArray());
        $response->assertStatus(401);
        $response->assertUnauthorized();
    }

    /**
     * @group commentAuth
     * @return void
     */
    public function test_not_auth_user_trying_update_comment(): void
    {
        $user = User::factory()->create();
        $article = Comment::factory()->create([
            'body' => $this->faker->sentence(1, rand(3, 9)),
            'rating' => $this->faker->paragraph(),
            'user_id' => $user->id
        ]);

        $response = $this->putJson(route('comments.update', $article->id), $article->toArray());
        $response->assertStatus(401);
        $response->assertUnauthorized();
    }

    /**
     * @group commentAuth
     * @return void
     */
    public function test_not_auth_user_trying_view_comment(): void
    {
        $user = User::factory()->create();
        $comment = Comment::factory()->create([
            'body' => $this->faker->sentence(1, rand(3, 9)),
            'rating' => $this->faker->paragraph(),
            'user_id' => $user->id
        ]);

        $response = $this->getJson(route('comments.show', $comment->id), $comment->toArray());
        $response->assertStatus(401);
        $response->assertUnauthorized();
    }



    /**
     * @group commentAuth
     * @return void
     */
    public function test_not_auth_user_trying_delete_comment(): void
    {
        $user = User::factory()->create();
        $comment = Comment::factory()->create([
            'body' => $this->faker->sentence(1, rand(3, 9)),
            'rating' => $this->faker->paragraph(),
            'user_id' => $user->id
        ]);

        $response = $this->deleteJson(route('comments.destroy', $comment->id), $comment->toArray());
        $response->assertStatus(401);
        $response->assertUnauthorized();
    }
}
