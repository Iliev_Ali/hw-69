<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\Models\User::factory()->count(3)->create();
        foreach ($users as $user) {
            \App\Models\Article::factory()->count(rand(5, 9))->for($user)->create();
        }
        $articles = \App\Models\Article::all();
        foreach ($articles as $article)
        {
            \App\Models\Comment::factory()->count(4)->for($article)->for($users->random())->create();
        }
    }
}
