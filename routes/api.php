<?php

use App\Http\Controllers\Auth\ApiAuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Public routes
Route::apiResource('articles', App\Http\Controllers\Api\ArticlesController::class)->only('index');
Route::apiResource('comments', App\Http\Controllers\Api\CommentsController::class)->only('index');

Route::post('/register', [ApiAuthController::class, 'register']);
Route::post('/login', [ApiAuthController::class, 'login']);

// Protected routes
Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::apiResource('articles', App\Http\Controllers\Api\ArticlesController::class)->except('index');
    Route::apiResource('comments', App\Http\Controllers\Api\CommentsController::class)->except('index');

    Route::delete('/logout', [ApiAuthController::class, 'logout']);
});
